require("coffee-script").register();


var config = require("./config.json");
var app    = require("./vpn-monitor");
var port   = 3333;
var ip     = config.vpn_bind_ip;


app.listen(port, ip);
console.log("RVK VPN Status Monitor Listening on", ip || "*", port);


process.on("uncaughtException", function (err) {
  console.error(err);
  console.log(err.stack);
});