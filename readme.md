# RVK VPN Monitor
is an tool to show RVK Gateways connected to an VPN. In opens an HTTP Server on Port 3333 to present an status table.

## dependecies
* running openvpn service as server
* node.js
* npm

## start running on Linux

* Create Your VPN with StatusFile to Parse
* Clone this pice of Software
* Configure ifPrefix of your VPN e.g. 10.11.12. in the source file vpn-monitor.coffee
* Run following Commands in a Linux type shell

```sh

# Enter directory
$ cd rvk-vpn-monitor
# Install software libs
$ npm update
# start http server
$ node vpn-monitor-server.js

```

## start running on Windows

tested with node.js v0.12.2

### Steps to be completed

* Clone with git from Bitbucket in a Folder of your Choice
* Install required Libraries via NPM (Node Packet Manager)
* configure config.json
** Bind IP - IP the service will listen (i.e. 10.8.0.1)
** Path of vpn-status.log (i.e. C:/Programme/openvpn/log/openvpn-status.log)
** IP Pattern (i.e. 10.8.0.) of the configured VPN 
* Enter Location of vpn-monitor and do a teststart
* Check StatusPage on Port 3333 within vpn Network (Use the Bind-IP of this service)
**  i.e. 10.8.0.1:3333
* Install forever.js using npm
* Stop service and Start again with forever to ensure longtime running
* Make an Entry to Windows Autostart to start service on Boot of Windows

### Steps as Commands for Windows Shell

```sh
# Go to Location of your Choice
cd C:\LocationOfMyChoice
# Clone Repository with git
git clone https://Neptuntriton@bitbucket.org/Neptuntriton/vpn-monitor.git
# Enter new directory
$ cd vpn-monitor
# Install software libs
$ npm install
# reconfigure config.json and save
notepad config.json
# start http service
node vpn-monitor-server.js
# Open Browser and check Page
iexplore http://10.8.0.1:3333 
# Stop Service
CTRL + c
# Install forever.js globaly 
npm install forever -g
# start service using forever
forever -c node vpn-monitor-server.js
# stop service
forever stopall

```
