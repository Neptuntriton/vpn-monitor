fs          = require("fs")
express     = require("express")
jade        = require("jade")
http        = require("http")
async       = require("async") 
lodash      = require("lodash")
config      = require("./config.json")

# global set up
GLOBAL._    = lodash
bindIP      = config.vpn_bind_ip                # '0.0.0.0'
ifPrefix    = config.vpn_ip_pattern             # '10.11.12.'
statusFile  = config.vpn_status_filee_location  # '/etc/openvpn/openvpn-status.log'
app         = express()


calcDiskSpace = (someClients) ->
  for i in [0...someClients.length]
    if someClients[i].status?.FreeDiskSpace?
      someClients[i].status.FreeDiskSpace.forEach (disk) ->        
        disk.load = Math.round(1000 * (1 - parseFloat((disk.free.match(/[-+]?[0-9]*\.?[0-9]+/)[0]) / parseFloat(disk.size.match(/[-+]?[0-9]*\.?[0-9]+/)[0])))) / 10
        


app.get "/", (req, res) ->
   
  async.waterfall([
    
    (callback) -> 
      filename       = statusFile
      try 
        openVPNLogFile = fs.lstatSync(filename)
        if openVPNLogFile?
          content = []
          fileContent = fs.readFileSync(filename, "utf8").split("\n")
          for i in [0...fileContent.length]
            if fileContent[i].indexOf(ifPrefix) > -1
              anIP   = fileContent[i].split(',')[0]
              client = {
                vpn_ip    : anIP
                real_ip   : fileContent[i].split(',')[2]
                last_time : fileContent[i].split(',')[3]
              }
              content.push client

          callback(null, content)
        else
          calback(null, "File does not exist!")
      catch e
        callback(e)
     
    (connectedClients, callback) ->


      async.each(connectedClients, (anClient, innerCallBack) ->
        
        console.log "Try to Call " + anClient.vpn_ip

        http.request({ host : anClient.vpn_ip, path : '/getID', method : 'GET'}, (res) ->
          #console.log('STATUS: ' + res.statusCode);
          #console.log('HEADERS: ' + JSON.stringify(res.headers));
          res.setEncoding('utf8')
          res.on 'data', (chunk) -> 
            if (res.statusCode == 200) && (res.headers["content-type"]?.indexOf("application/json") > -1)
              anClient.status = JSON.parse(chunk)
            else 
              anClient.status = "Seems not to be an RVK Gateway!"  
            innerCallBack(null)
        ).on 'error', (err) ->          
          anClient.status = err
          innerCallBack(null)        
        .end()        
         

      , () ->        
        console.log "finished"        
        callback(null, connectedClients)
      )      


  ], (err, results) ->
    results = _.sortBy(results, (item) -> return item.status.ID )
    calcDiskSpace(results)
    html    = jade.renderFile('./templates/index.jade', connected_clients : results)
    res.send(html)
    #res.status(200).json({ connected_clients : results })
  )
  
module.exports = app
